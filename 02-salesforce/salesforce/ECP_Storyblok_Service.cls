public class ECP_Storyblok_Service {

    /**
     * Gets the currently synced content version number
     * @return String Version number
     */
    public static String getVersion() {
        String version = '';

        List<Storyblok__Content__c> versions = [
            SELECT
                Storyblok__body__c
            FROM
                Storyblok__Content__c
            WHERE
                Storyblok__slug__c = 'version'
            LIMIT 1];

        if (!versions.isEmpty()) {
            version = versions[0].Storyblok__body__c;
        }

        return version;
    }

    /**
     * Gets all content items of a Storyblok content folder.
     * @param  slug Slug of the content folder
     * @return      JSON String
     */
    public static String getAll(String slug) {
        String version = getVersion();
        String storyblokFolderSlug = version + '/' + slug + '%';
        String result = '[]';

        Datetime now = Datetime.now();
        Long nowFormatted = now.getTime();

        List<Storyblok__Content__c> contents = [
            SELECT
                Storyblok__body__c,
                Storyblok__name__c,
                Storyblok__euid__c
            FROM
                Storyblok__Content__c
            WHERE
                Storyblok__slug__c LIKE :storyblokFolderSlug
                AND (Storyblok__publish_at__c = null OR Storyblok__publish_at__c = 0 OR Storyblok__publish_at__c < :nowFormatted)
                AND (Storyblok__expire_at__c = null OR Storyblok__expire_at__c = 0 OR Storyblok__expire_at__c > :nowFormatted)
            ORDER BY Storyblok__position__c];

        if (!contents.isEmpty()) {
            List<Object> contentList = new List<Object>();

            for(Storyblok__Content__c content :contents) {
                Map<String, Object> storyblokJsonBody = (Map<String, Object>)JSON.deserializeUntyped(content.Storyblok__body__c);
                Map<String, Object> storyblokContentObject = new Map<String, Object>();
                storyblokContentObject.put('body', storyblokJsonBody);
                storyblokContentObject.put('name', content.Storyblok__name__c);
                storyblokContentObject.put('id', content.Storyblok__euid__c);
                contentList.add(storyblokContentObject);
            }

            result = JSON.serialize(contentList);
        }

        return result;
    }

    /**
     * Gets a single content item
     * @param  slug Slug of the content item
     * @return      JSON String
     */
    public static String getSingle(String slug) {
        String version = getVersion();
        String storySlug = version + '/' + slug;
        Datetime now = Datetime.now();
        Long nowFormatted = now.getTime();

        List<Storyblok__Content__c> contents = [
            SELECT
                Storyblok__body__c
            FROM
                Storyblok__Content__c
            WHERE
                Storyblok__slug__c = :storySlug
                AND (Storyblok__publish_at__c = null OR Storyblok__publish_at__c = 0 OR Storyblok__publish_at__c < :nowFormatted)
                AND (Storyblok__expire_at__c = null OR Storyblok__expire_at__c = 0 OR Storyblok__expire_at__c > :nowFormatted)
            LIMIT 1];

        if (!contents.isEmpty()) {
            Map<String, Object> storyblokJsonBody = (Map<String, Object>)JSON.deserializeUntyped(contents[0].Storyblok__body__c);
            return JSON.serialize(storyblokJsonBody);
        }

        return 'null';
    }
}