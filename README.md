# Storyblok Workshop

This is the companion repository to the Storyblok Workshop.

You can also view the slides [here](https://docs.google.com/presentation/d/11rSYl-T9r3XUkxxMugVxWFqeA6c3fYC9JCO-ZwbkmZI/edit?usp=sharing).

## Prerequisites

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/en/) version 9+

## Code required for the preview

\1. Data fetching

~~~
import StoryblokInitializer from './util/storyblok'

if (typeof StoryblokSf !== 'undefined') {
  var slug = 'brands/...'

  StoryblokSfClient.getContent(slug, (response) => {
    console.log(response)
    // assign variable 'response' to vue.js here
  }, (error) => {
    console.error(error)
  })

  StoryblokInitializer(() => {
  	StoryblokSfClient.getContent(slug, (response) => {
      console.log(response)
    })
  })
} else {
  // use data from salesforce
}
~~~

\2. Event registering  
Create a file in util/storyblok.js with following content.

~~~
export default function StoryblokInitializer(cb) {
  StoryblokSf.bridge.resetEvents()

  StoryblokSf.bridge.on('change', () => {
    cb()
  })

  StoryblokSf.bridge.on('published', (data) => {
    StoryblokSf.bridge.get({slug: data.slug, version: 'published'}, (data) => {
      StoryblokSf.createOrUpdateContentItem(data.story)
    })

    cb()
  })

  StoryblokSf.bridge.on('unpublished', (data) => {
    StoryblokSf.deleteItem(data.slug)
  })

  StoryblokSf.bridge.on('customEvent', (data) => {
    if (data.event == 'start-sync') {
      StoryblokSf.startSync()
    }
  })
}
~~~