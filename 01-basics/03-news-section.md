# 1.2 Build a news section

## Goal

Build a news section with overview and detail.


## Instructions

Create the file news_overview.hbs

```
{{{ _editable }}}
<div>
  <h1>News overview</h1>
  <div id="news">
  </div>
</div>
```

Add the access token to the main.hbs

```
storyblok.init({accessToken: 'nmhc6FGnwUHwO6XMR6K2Mwtt'})
```

Add the api call to receive the news for the home section

```
var el = document.querySelector('#news')
if (el) {
  storyblok.getAll({starts_with: 'news', version: 'draft'}, function(data) {
    for (var i = 0; i < data.stories.length; i++) {
      el.innerHTML += '<a href="/' + data.stories[i].full_slug + '?_storyblok=1">' + data.stories[i].content.title + '</a>'
    }
  })
}
```