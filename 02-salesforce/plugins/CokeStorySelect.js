const Fieldtype = {
  mixins: [window.Storyblok.plugin],
  template: `<div>
  <select class="uk-width-1-1" v-model="model.value">
    <option></option>
    <option :key="story.id" :value="{name: story.name, uuid: story.uuid, full_slug: story.full_slug}" v-for="story in stories">
      {{ story.name }}
    </option>
  </select>
  </div>`,
  data() {
    return {
      stories: []
    }
  },
  methods: {
    initWith() {
      return {
        plugin: 'coke-story-select',
        value: {}
      }
    },
    getOption(name, fallback) {
      if (!this.schema.options) {
        return fallback
      }
      for (var i = 0; i < this.schema.options.length; i++) {
        if (this.schema.options[i].name == name) {
          return this.schema.options[i].value
        }
      }
      return fallback
    }
  },
  events: {
    'plugin:created': function() {
      console.log('plugin:created')

      let story = this.$store.model.full_slug ? this.$store.model : {full_slug: 'test/test'}
      let slugs = story.full_slug.split('/')
      let level = parseInt(this.getOption('level', '0'))
      let folder = this.getOption('folder', '')
      let slug = ''
      
      for (var i = 0; i < level; i++) {
        slug += slugs[i] + '/'
      }
      
      slug += folder

      this.api(`spaces/${this.spaceId}/stories`).get({story_only: 1, starts_with: slug}).then((res) => {
        this.stories = res.data.stories
      })
    },
    'plugin:destroyed': function() {
      console.log('plugin:destroyed')
    }
  },
  watch: {
    'model': {
      handler: function (value) {
        this.$emit('changed-model', value);
      },
      deep: true
    }
  }
}