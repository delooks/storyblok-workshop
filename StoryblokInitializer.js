// Salesforce client
import 'babel-polyfill'
import StoryblokClient from 'storyblok-js-client'

var SalesforceClient = {
  bridge: window.Storyblok,
  api: null,
  config: {},
  syncRunning: false,
  version: 'published',

  init: function(config) {
    this.bridge.init(config)
    this.contentModel = new StoryBlok.Storyblok__Content__c()
    this.setVersion()
    this.config = config || {}

    if (config.accessToken) {
      this.api = new StoryblokClient({accessToken: config.accessToken, headers: {}})
    }

    var newEl = document.createElement('div')
    newEl.innerHTML = '<div data-storyblok-loading="1" style="display:none;position:fixed;top:0;left:0;right:0;bottom:0;background:rgba(0,0,0,0.7);z-index:9900;text-align:center;"><div style="color:#FFF;padding:200px;">Syncing content...<br>This process can take a few minutes.<div data-storyblok-loading-process></div></div></div>'
    document.body.appendChild(newEl)
  },

  initClient(config) {
    this.api = new StoryblokClient({accessToken: config.accessToken, headers: {}})
  },

  setVersion: function() {
    if (window.location != window.parent.location) {
      this.version = 'draft'
    }
  },

  async getVersion() {
    var contents = await this.retrieveContent({where: {Storyblok__slug__c: {eq: 'version'}}, limit: 1})
    var contentVersion = ''

    if (contents.length > 0) {
      contentVersion = contents[0].get('Storyblok__body__c')
    }
    return contentVersion
  },

  editMode() {
    return window.location != window.parent.location || window.location.href.indexOf('_storyblok=') > -1
  },

  async getContent(slug, success, error) {
    if (typeof slug !== 'string') {
      success(slug)
      return this
    }

    if (this.editMode()) {
      this.bridge.get({slug: slug, version: 'draft'}, (data) => {
        var contentItem = {
          body: data.story.content,
          slug: data.story.full_slug,
          name: data.story.name,
          id: data.story.id,
          uuid: data.story.uuid
        }
        success(contentItem)
      }, () => {
        error({error: 'Not found'})
      })
    } else {
      var contentVersion = await this.getVersion()
      slug = contentVersion + '/' + slug

      this.contentModel.retrieve({where: {
          Storyblok__slug__c: {eq: slug}
        }, limit: 1}, function(err, records) {

        if (err != null) {
          return error(err)
        }

        if (records.length > 0) {
          var contentItem = {
            body: JSON.parse(records[0].get('Storyblok__body__c').replace(new RegExp('&quot;', 'g'), '"')),
            slug: records[0].get('Storyblok__slug__c'),
            name: records[0].get('Storyblok__name__c')
          }
          success(contentItem)
        } else {
          error({error: 'Not found'})
        }
      })
    }
  },

  async getContents(slug, limit, page, success, error) {
    if (this.editMode()) {
      var options = {
        per_page: limit,
        page: page,
        version: 'draft'
      }

      if (slug.constructor === Object) {
        options = Object.assign(options, slug)
      } else {
        options.starts_with = slug
      }

      this.bridge.getAll(options, function(data) {
        var results = []

        for (var i = 0; i < data.stories.length; i++) {
          var contentItem = {
            body: data.stories[i].content,
            slug: data.stories[i].full_slug,
            name: data.stories[i].name,
            id: data.stories[i].id,
            uuid: data.stories[i].uuid
          }
          results.push(contentItem)
        }

        success(results)
      })
    } else {
      var contentVersion = await this.getVersion()
      slug = contentVersion + '/' + slug

      var query = {where: {Storyblok__slug__c: {like: slug + '%'}},
                  orderby: [{Storyblok__position__c: 'ASC'}],
                  limit: limit}

      if (page > 1) {
        query.offset = page * limit
      }

      this.contentModel.retrieve(query, function(err, records) {
        if (err != null) {
          return error(err)
        }

        if (records.length > 0) {
          var results = []

          for (var i = 0; i < records.length; i++) {
            var contentItem = {
              body: JSON.parse(records[i].get('Storyblok__body__c').replace(new RegExp('&quot;', 'g'), '"')),
              slug: records[i].get('Storyblok__slug__c'),
              name: records[i].get('Storyblok__name__c')
            }
            results.push(contentItem)
          }

          success(results)
        } else {
          success([])
        }
      })
    }
  },

  retrieveContent(query) {
    return new Promise((success, error) => {
      this.contentModel.retrieve(query, (err, records) => {
        if (err != null) {
          console.log(err)
          error(err)
          return
        }
        success(records)
      })
    })
  },

  deleteContent(ids) {
    return new Promise((success, error) => {
      this.contentModel.del(ids, (err, records) => {
        if (err != null) {
          console.log(err)
          error(err)
          return
        }
        success(records)
      })
    })
  },

  async startSync() {
    if (this.syncRunning) {
      return
    }
    this.syncRunning = true
    var loading = document.querySelector('[data-storyblok-loading]')
    loading.style.display = 'block'

    this.syncVersion = new Date().getTime().toString()
    await this.getAllStories()
    await this.setSyncVersion()
    await this.cleanup()

    loading.style.display = 'none'
    this.syncRunning = false
  },

  async cleanup() {
    this.updateProcess(0, 1, 'Cleanup content')

    var contents = await this.retrieveContent({where: {
      Storyblok__content_version__c: {ne: this.syncVersion},
      Storyblok__slug__c: {ne: 'version'}
    }, limit: 100})

    if (contents.length > 0) {
      var ids = []
      for (var i = 0; i < contents.length; i++) {
        ids.push(contents[i].get('Id'))
      }

      await this.deleteContent(ids)

      if (contents.length == 100) {
        this.cleanup()
      }
    }
  },

  async setSyncVersion() {
    var contents = await this.retrieveContent({where: {Storyblok__slug__c: {eq: 'version'}}, limit: 1})

    if (contents.length > 0) {
      contents[0].set('Storyblok__body__c', this.syncVersion)
      contents[0].update()
    } else {
      this.contentModel.create({
        'Storyblok__body__c': this.syncVersion,
        'Storyblok__slug__c': 'version'
      }, (error) => {
        if (error) {
          console.error(error)
        } else {
          console.log('Version created')
        }
      })
    }
  },

  updateProcess(current, all, step) {
    const processDiv = document.querySelector('[data-storyblok-loading-process]')
    processDiv.innerHTML = step + ' - ' + current + ' of ' + all + ' complete'
  },

  async getAllStories(token) {
    var page = 1
    var res = await this.api.get('cdn/stories', {
      per_page: 25,
      page: page,
      token: token
    })
    var all = res.data.stories
    var total = res.total
    var lastPage = Math.ceil((res.total / 25))

    if (total > 25) {
      while (page < lastPage){
        page++
        res = await this.api.get('cdn/stories', {
          per_page: 25,
          page: page,
          token: token
        })
        res.data.stories.forEach((story) => {
          all.push(story)
        })
        this.updateProcess(page, lastPage, 'Getting content')
      }      
    }

    for (var i = 0; i < all.length; i++) {
      await this.createContentItem(all[i])
      this.updateProcess(i, all.length, 'Publishing content')
    }

    return all
  },

  assignContentValues(record, content) {
    record.set('Storyblok__body__c', JSON.stringify(content.content))
    record.set('Storyblok__slug__c', this.syncVersion + '/' + content.full_slug)
    record.set('Storyblok__name__c', content.name)
    record.set('Storyblok__position__c', content.position)
    record.set('Storyblok__euid__c', content.id.toString())
    record.set('Storyblok__uuid__c', content.uuid)
    record.set('Storyblok__content_version__c', this.syncVersion)

    if (typeof this.config.customFields !== 'undefined') {
      this.config.customFields.forEach((customField) => {
        if (typeof content.content[customField.schemaKey] !== 'undefined') {
          if (customField.type == 'timestamp') {
            if (content.content[customField.schemaKey].length == 16) {
              record.set(customField.sfName, Date.parse(content.content[customField.schemaKey]))
            } else {
              record.set(customField.sfName, 0)
            }
          } else {
            record.set(customField.sfName, content.content[customField.schemaKey])
          }
        }
      })
    }
  },

  createContentItem(content) {
    return new Promise((success, error) => {
      let obj = {
        Storyblok__body__c: JSON.stringify(content.content),
        Storyblok__slug__c: this.syncVersion + '/' + content.full_slug,
        Storyblok__name__c: content.name,
        Storyblok__position__c: content.position,
        Storyblok__euid__c: content.id.toString(),
        Storyblok__uuid__c: content.uuid,
        Storyblok__content_version__c: this.syncVersion
      }

      if (typeof this.config.customFields !== 'undefined') {
        this.config.customFields.forEach((customField) => {
          if (typeof content.content[customField.schemaKey] !== 'undefined') {
            if (customField.type == 'timestamp') {
              if (content.content[customField.schemaKey].length == 16) {
                obj[customField.sfName] = Date.parse(content.content[customField.schemaKey])
              } else {
                obj[customField.sfName] = 0
              }
            } else {
              obj[customField.sfName] = content.content[customField.schemaKey]
            }
          }
        })
      }

      this.contentModel.create(obj, (cError) => {
        if (cError) {
          error(cError)
        } else {
          console.log(content.full_slug + ' created')
          success(content)
        }
      })
    })
  },

  async createOrUpdateContentItem(content) {
    var id = content.id.toString()
    this.syncVersion = await this.getVersion()

    this.contentModel.retrieve({where: {Storyblok__euid__c: {eq: id}, Storyblok__content_version__c: {eq: this.syncVersion}}, limit: 1}, (err, records) => {
      if (err != null) {
        console.log(err)
        return
      }

      if (records.length > 0) {
        this.assignContentValues(records[0], content);
        records[0].update()
      } else {
        this.assignContentValues(this.contentModel, content)
        this.contentModel.create()
      }
    })
  },

  async deleteItem(slug) {
    var contentVersion = await this.getVersion()

    this.contentModel.retrieve({where: {
        Storyblok__slug__c: {eq: contentVersion + '/' + slug}
      }, limit: 1}, function(err, records) {

      if (err != null) {
        console.log(err)
        return
      }

      if (records.length > 0) {
        records[0].del()
      } else {
        console.log('Record not found to unpublish')
      }
    })
  }
}

window.StoryblokSf = SalesforceClient

// Rendering engine with handlebars

import snarkdown from 'snarkdown'

var StoryblokSfClientObj = {
  templates: [],

  init(config) {
    if (typeof Handlebars === 'undefined' || typeof $ === 'undefined') {
      console.error('Handlebars and jQuery are required')
    } else {
      if (this.editMode()) {
        StoryblokSf.init(config)
      }

      this.registerHelper()
      this.compileTemplates()
    }
  },

  editMode() {
    return window.location != window.parent.location || window.location.href.indexOf('_storyblok=') > -1
  },

  compileTemplates() {
    this.templates = []

    $('[data-sb-template]').each((index, el) => {
      this.templates[$(el).data('sb-template')] = Handlebars.compile($(el).html())
    })
  },

  registerHelper() {
    Handlebars.registerHelper('component', (data) => {
      if (typeof this.templates[data.component] === 'undefined') {
        console.error('Storyblok: Component ' + data.component + ' not found.')
        return new Handlebars.SafeString('')
      }

      var result = this.templates[data.component](data)
      result = (this.editMode() ? data._editable : '') + result
      return new Handlebars.SafeString(result)
    })

    Handlebars.registerHelper('markdown', (data) => {
      return new Handlebars.SafeString(this.markdown(data))
    })
  },

  markdown(data) {
    if (typeof snarkdown !== 'undefined') {
      return snarkdown(data)
    }
    return data
  },

  getContent(data, success, error) {
    if (this.editMode()) {
      StoryblokSf.getContent(data, success, error)
    } else {
      if (typeof data !== 'undefined' && typeof data.body !== 'undefined' && typeof data.body.component !== 'undefined') {
        success(data)
      } else {
        error('Storyblok: Root component not found')
      }
    }
  },

  reRender(element, slug, cb) {
    this.getContent(slug, (data) => {
      var rootEditable = (this.editMode() ? data.body._editable : '')

      if (typeof this.templates[data.body.component] === 'undefined') {
        console.error('Storyblok: Component ' + data.body.component + ' not found.')
        return
      }

      $(element).html(rootEditable + this.templates[data.body.component](data.body))

      if (this.editMode()) {
        StoryblokSf.bridge.enterEditmode()
      }

      cb(data);
    }, (error) => {
      console.log(error)
    })
  },

  render(element, slug, cb) {
    this.reRender(element, slug, cb)

    if (this.editMode()) {

      if (window.location != window.parent.location || window.location.href.indexOf('_storyblok=') > -1) {
        // editmode config start
        StoryblokSf.bridge.resetEvents()

        StoryblokSf.bridge.on('change', () => {
          // rerender
        })

        StoryblokSf.bridge.on('published', (data) => {
          StoryblokSf.bridge.get({slug: data.slug, version: 'published'}, (data) => {
            StoryblokSf.createOrUpdateContentItem(data.story)
          })

          // rerender
        })

        StoryblokSf.bridge.on('unpublished', (data) => {
          StoryblokSf.deleteItem(data.slug)
        })

        StoryblokSf.bridge.on('customEvent', (data) => {
          if (data.event == 'start-sync') {
            StoryblokSf.startSync()
          }
        })
        // editmode config end
      } else {
        // use data from salesforce
      }
    }
  }
}

window.StoryblokSfClient = StoryblokSfClientObj