# Goal

At the end of this workshop you will have a website with content managed by Storyblok.

# Preparation

* You should have installed the Storyblok managed package on your org:

Live:  
https://login.salesforce.com/packaging/installPackage.apexp?p0=04t58000000TCTz  

Sandbox:  
https://test.salesforce.com/packaging/installPackage.apexp?p0=04t58000000TCTz

# Installation

After installing the managed package by calling the link above you can start with the integration.

On every page that's managed by the Storyblok editor you need to include the following code to include the Javascript bridge to the CMS.  
In our workshop we included that piece of code in `ECP_Template.page`. It may be only included on the page ECP_StoryblokPage.page

```
<apex:remoteObjects jsNamespace="StoryBlok">
  <apex:remoteObjectModel name="Storyblok__Content__c" fields="Id,Storyblok__body__c,Storyblok__slug__c,Storyblok__euid__c,Storyblok__name__c,Storyblok__position__c,Storyblok__content_version__c,Storyblok__publish_at__c,Storyblok__expire_at__c,Storyblok__uuid__c"/>
</apex:remoteObjects>

<script type="text/javascript" src="//app.storyblok.com/f/storyblok-salesforce.js"></script>
```

## The ECP_StoryblokPage.page

Because the path of the url is not the same that we use in Storyblok we created the page `ECP_StoryblokPage.page` to acts as a proxy for all content pages that are editable in the Storyblok editor.  
It needs the parameter `page` and the parameter `_storyblok_c` from the Storyblok editor iframe to include the right Salesforce pages.

```
<apex:page id="ECP_StoryblokPage" docType="html-5.0" showHeader="false" applyHTMLTag="false" sidebar="false" standardStylesheets="false">
    <apex:include pageName="ECP_{!$CurrentPage.parameters._storyblok_c}"/>
</apex:page>
```

## The brand detail page

We created the component `ECP_StoryblokInitializer.component` to be able to share the initializer code.  

* YOUR_VISUALFORCE_SUBDOMAIN needs to be replaced by your visualforce subdomain.
* YOUR_PREVIEW_TOKEN needs to be replaced by 


```
<apex:component>
  <script type="text/javascript"> 
    StoryblokSfClient.init({
        accessToken: 'YOUR_PREVIEW_TOKEN',
        customParent: 'https://storyblok.YOUR_VISUALFORCE_SUBDOMAIN.visual.force.com',
        customFields: [
          {sfName: 'Storyblok__publish_at__c', schemaKey: 'publish_at', type: 'timestamp'},
          {sfName: 'Storyblok__expire_at__c', schemaKey: 'expire_at', type: 'timestamp'}
        ]
    })

    Handlebars.registerHelper('image', function(image, option) {
        var imageService = '//img2.storyblok.com/';
        var path = image.replace('//a.storyblok.com', '');
        return new Handlebars.SafeString(imageService + option + path);
    })
  </script>
</apex:component>
```

We extended the component `ECP_BrandDetail.component` and included the `<c:ECP_StoryblokInitializer />` component that 
needs to be inserted between the template definitions and the backbone initialization code.  
We added a code to check if the user is viewing the page inside the editor or live.  
When the user is viewing the live version we get the data directly from the {!storyblokContent} variable which is defined in the `ECP_Brand_Controller.cls`.

```
<script data-sb-template="BrandDetail" type="text/template">
  ...
</script>

<c:ECP_StoryblokInitializer />

<script type="text/javascript"> 

jQuery(function($){

    var BrandDetailView = Backbone.View.extend({
        initialize: function() {
            var slug = '{!$CurrentPage.parameters.page}';

            if (window.location.host == 'ecpdev6-serviceportal.cs19.force.com') {
                slug = { body: {!storyblokContent} };
                console.log('{!storyblokSlug}')
            }

            StoryblokSfClient.render('.brandSection', slug, this.afterRender);
        },
        afterRender: function() {
            console.log('ready');
        }
    })

    new BrandDetailView();

});

</script>
```

## Custom Fields

### Publish at and expire at date

To be able to define a publish and expire date we have added the fields publish_at and expire_at in the schema definition of the BrandDetail content type. We have added the mapping to those fields to the ECP_StoryblokInitializer.component file. This mapping makes sure to add the dates in Storyblok to the fields of the custom object in Salesforce.

```
StoryblokSfClient.init({
  ...
  customFields: [
    {sfName: 'Storyblok__publish_at__c', schemaKey: 'publish_at', type: 'timestamp'},
    {sfName: 'Storyblok__expire_at__c', schemaKey: 'expire_at', type: 'timestamp'}
  ]
})
```

### Transformation

Sometimes you want to transform a field before passing to Salesforce. Therefore you can pass a transform function to the customFields.

```
StoryblokSfClient.init({
  ...
  customFields: [
    {sfName: 'SubtradeChannel__c', schemaKey: 'channel', type: 'array', transform: function(value) {
      return value.keys
    }}
  ]
})
```

## The brand overview page

For the brand overview page we created the `ECP_BrandOverview.page` page and the `ECP_BrandOverview.component` component.  
The component has the controller `ECP_Brand_Overview_Controller.cls` defined.

In the controller we used the Storyblok service class to fetch the content entries directly from Salesforce.

```
String locale = 'es_ES';
String storefront = 'iberia_CCIP';

this.brands = ECP_Storyblok_Service.getAll(storefront + '/' + locale + '/brands');
this.categories = ECP_Storyblok_Service.getAll(storefront + '/' + locale + '/categories');
```

## The Storyblok_Service class

The `ECP_Storyblok_Service.cls` class has two methods.

1. `ECP_Storyblok_Service.getAll(slug)` fetches multiple content items. Example: ECP_Storyblok_Service.getAll('es_ES/iberia_CCIP/brands')
2. `ECP_Storyblok_Service.getSingle(slug)` fetches a single content items. Example: ECP_Storyblok_Service.getSingle('es_ES/iberia_CCIP/brands/coke')

See the ECP_Storyblok_Service.cls file in the salesforce folder of this git repository.

## Syncing data after publishing

When a user edits a Story in Storyblok and clicks the "Publish" button the content item automatically gets saved to Salesforce.  
But there are situations where the user needs to start the syncing process manually.

### When the user needs to start the sync process manually?

* When the user deletes a Story or multiple Stories
* When the user publishes multiple Stories at once using the multi select in the Stories list view
* When the user changes the environment

### How to start the sync process

Go to any page that has a visual preview and click the "Start sync" button in the submenu: 

![Starting the sync process](https://monosnap.com/file/bfz1AjyVDZWMfUprARai2anHHKm76J.png)

# Custom Javascript helpers of the Storyblok Salesforce client

There has been developed some javascript helpers for this project which can be overwritten redefining the `window.StoryblokSfClient` javascript object.  
We recommend to put that code inside your javascript bundle.

```
import snarkdown from 'snarkdown'

var StoryblokSfClientObj = {
  templates: [],

  init(config) {
    if (typeof Handlebars === 'undefined' || typeof $ === 'undefined') {
      console.error('Handlebars and jQuery are required')
    } else {
      if (this.editMode()) {
        StoryblokSf.init(config)
      }

      this.registerHelper()
      this.compileTemplates()
    }
  },

  editMode() {
    return window.location != window.parent.location || window.location.href.indexOf('_storyblok=') > -1
  },

  compileTemplates() {
    this.templates = []

    $('[data-sb-template]').each((index, el) => {
      this.templates[$(el).data('sb-template')] = Handlebars.compile($(el).html())
    })
  },

  registerHelper() {
    Handlebars.registerHelper('component', (data) => {
      if (typeof this.templates[data.component] === 'undefined') {
        console.error('Storyblok: Component ' + data.component + ' not found.')
        return new Handlebars.SafeString('')
      }

      var result = this.templates[data.component](data)
      result = (this.editMode() ? data._editable : '') + result
      return new Handlebars.SafeString(result)
    })

    Handlebars.registerHelper('markdown', (data) => {
      return new Handlebars.SafeString(this.markdown(data))
    })
  },

  markdown(data) {
    if (typeof snarkdown !== 'undefined') {
      return snarkdown(data)
    }
    return data
  },

  getContent(data, success, error) {
    if (this.editMode()) {
      StoryblokSf.getContent(data, success, error)
    } else {
      if (typeof data !== 'undefined' && typeof data.body !== 'undefined' && typeof data.body.component !== 'undefined') {
        success(data)
      } else {
        error('Storyblok: Root component not found')
      }
    }
  },

  reRender(element, slug, cb) {
    this.getContent(slug, (data) => {
      var rootEditable = (this.editMode() ? data.body._editable : '')

      if (typeof this.templates[data.body.component] === 'undefined') {
        console.error('Storyblok: Component ' + data.body.component + ' not found.')
        return
      }

      $(element).html(rootEditable + this.templates[data.body.component](data.body))

      if (this.editMode()) {
        StoryblokSf.bridge.enterEditmode()
      }

      cb(data);
    }, (error) => {
      console.log(error)
    })
  },

  render(element, slug, cb) {
    this.reRender(element, slug, cb)

    if (this.editMode()) {
      StoryblokSf.bridge.resetEvents()

      StoryblokSf.bridge.on('change', () => {
        this.reRender(element, slug, cb)
      })

      StoryblokSf.bridge.on('published', (data) => {
        StoryblokSf.bridge.get({slug: data.slug, version: 'published'}, (data) => {
          StoryblokSf.createOrUpdateContentItem(data.story)
        })

        this.reRender(element, slug, (cbData) => {
          cb(cbData)
        })
      })

      StoryblokSf.bridge.on('unpublished', (data) => {
        StoryblokSf.deleteItem(data.slug)
      })

      StoryblokSf.bridge.on('customEvent', (data) => {
        if (data.event == 'start-sync') {
          StoryblokSf.startSync()
        }
      })
    }
  }
}

window.StoryblokSfClient = StoryblokSfClientObj
```

# How to add a custom field which you can query in APEX

To be able to query Storyblok's content types by a custom field in APEX following steps are necessary: 

\1. Add a field in the schema of the content type in Storyblok. Example: custom_field  
\2. Add a custom field on the Storyblok content object in Salesforce. Example: custom_field__c  
\3. Add the field to the Storyblok remote object in ECP_Template.page at

```
<apex:remoteObjects jsNamespace="StoryBlok">
  <apex:remoteObjectModel name="Storyblok__Content__c" fields="...,custom_field__c"/>
</apex:remoteObjects>
```

\4. Add the field mapping in ECP_StoryblokInitializer.component

```
StoryblokSfClient.init({
  ...
  customFields: [
    {sfName: 'custom_field__c', schemaKey: 'custom_field'},
  ]
})
```