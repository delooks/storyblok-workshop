# 1.1 Quickstart

## Goal

Connect Storyblok to your local dev environment and extend the quickstart project with a custom component.

## Instructions

* Create a new space
* Download the node js sample
* Start the server locally
* Try to add a custom component
