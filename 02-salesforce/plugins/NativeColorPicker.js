const Fieldtype = {
  mixins: [window.Storyblok.plugin],
  template: `<div>
  <input class="uk-width-1-1 uk-margin-bottom" v-model="model.color" type="color" />
  <input class="uk-width-1-1" v-model="model.color" /></div>`,
  methods: {
    initWith() {
      return {
        plugin: 'native-color-picker',
        color: '#f40000'
      }
    }
  },
  events: {
    'plugin:created': function() {
      console.log('plugin:created')
    },
    'plugin:destroyed': function() {
      console.log('plugin:destroyed')
    }
  },
  watch: {
    'model': {
      handler: function (value) {
        this.$emit('changed-model', value);
      },
      deep: true
    }
  }
}